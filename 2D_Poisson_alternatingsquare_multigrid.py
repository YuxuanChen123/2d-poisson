import numpy as np
import math

a=0.0
b=0.0
lx=3.0
ly=3.0
nx=10
ny=8
hx=1.0*lx/(nx)
hy=1.0*ly/(ny)

def f(x0,y0):
    return -2.0*(x0**2+y0**2-3*x0-3*y0)

def r(x00,y00):
    return 0.0

n=(nx-1)*(ny-1)
print "n=", n

knode=0
x_l=np.zeros((n,2))
for ix in range(1,nx):
    for iy in range(1,ny):
        x_l[knode][0]=a+1.0*ix*hx
        x_l[knode][1]=b+1.0*iy*hy
        knode=knode+1
        
n_s=[]
n_b=[]
if ny%2==0:
    for snode in range(0,n,2):
        n_s.append(snode)
    for bnode in range(1,n,2):
        n_b.append(bnode)
elif ny%2==1:
    kk=0
    for xnode in range(1,nx):
        for ynode in range(1,ny):
            if xnode%2==1:
                if ynode%2==1:
                    n_s.append(kk)
                elif ynode%2==0:
                    n_b.append(kk)
            elif xnode%2==0:
                if ynode%2==1:
                    n_b.append(kk)
                elif ynode%2==0:
                    n_s.append(kk)
            kk=kk + 1

ns=len(n_s)
nb=len(n_b)
tol=10**-10

areat=(hx*hy)/2.0
delta=2*areat

salpha=np.zeros((4,ns))
sbeta=np.zeros((4,ns))
sgamma=np.zeros((4,ns))
balpha=np.zeros((8,nb))
bbeta=np.zeros((8,nb))
bgamma=np.zeros((8,nb))

for lnodes in range(ns):
    npoints=n_s[lnodes]
    As=np.zeros((4,4))
    #T1
    As[0][0]=x_l[npoints][0]-hx
    As[0][1]=x_l[npoints][1]
    As[0][2]=x_l[npoints][0]
    As[0][3]=x_l[npoints][1]-hy
    #T2
    As[1][0]=x_l[npoints][0]
    As[1][1]=x_l[npoints][1]-hy
    As[1][2]=x_l[npoints][0]+hx
    As[1][3]=x_l[npoints][1]
    #T3
    As[2][0]=x_l[npoints][0]
    As[2][1]=x_l[npoints][1]+hy
    As[2][2]=x_l[npoints][0]-hx
    As[2][3]=x_l[npoints][1]
    #T4
    As[3][0]=x_l[npoints][0]+hx
    As[3][1]=x_l[npoints][1]
    As[3][2]=x_l[npoints][0]
    As[3][3]=x_l[npoints][1]+hy
    for iis in range(4):
        salpha[iis][lnodes]=1.0*(As[iis][0]*As[iis][3]-As[iis][2]*As[iis][1])/delta
        sbeta[iis][lnodes]=1.0*(As[iis][1]-As[iis][3])/delta
        sgamma[iis][lnodes]=1.0*(As[iis][2]-As[iis][0])/delta
        
    
for lnodeb in range(nb):
    npointb=n_b[lnodeb]
    Ab=np.zeros((8,4))
    #T1
    Ab[0][0]=x_l[npointb][0]-hx
    Ab[0][1]=x_l[npointb][1]
    Ab[0][2]=x_l[npointb][0]-hx
    Ab[0][3]=x_l[npointb][1]-hy
    #T2
    Ab[1][0]=x_l[npointb][0]-hx
    Ab[1][1]=x_l[npointb][1]-hy
    Ab[1][2]=x_l[npointb][0]
    Ab[1][3]=x_l[npointb][1]-hy
    #T3
    Ab[2][0]=x_l[npointb][0]
    Ab[2][1]=x_l[npointb][1]-hy
    Ab[2][2]=x_l[npointb][0]+hx
    Ab[2][3]=x_l[npointb][1]-hy
    #T4
    Ab[3][0]=x_l[npointb][0]+hx
    Ab[3][1]=x_l[npointb][1]-hy
    Ab[3][2]=x_l[npointb][0]+hx
    Ab[3][3]=x_l[npointb][1]
    #T5
    Ab[4][0]=x_l[npointb][0]+hx
    Ab[4][1]=x_l[npointb][1]
    Ab[4][2]=x_l[npointb][0]+hx
    Ab[4][3]=x_l[npointb][1]+hy
    #T6
    Ab[5][0]=x_l[npointb][0]+hx
    Ab[5][1]=x_l[npointb][1]+hy
    Ab[5][2]=x_l[npointb][0]
    Ab[5][3]=x_l[npointb][1]+hy
    #T7
    Ab[6][0]=x_l[npointb][0]
    Ab[6][1]=x_l[npointb][1]+hy
    Ab[6][2]=x_l[npointb][0]-hx
    Ab[6][3]=x_l[npointb][1]+hy
    #T8
    Ab[7][0]=x_l[npointb][0]-hx
    Ab[7][1]=x_l[npointb][1]+hy
    Ab[7][2]=x_l[npointb][0]-hx
    Ab[7][3]=x_l[npointb][1]
    for iib in range(8):
        balpha[iib][lnodeb]=1.0*(Ab[iib][0]*Ab[iib][3]-Ab[iib][2]*Ab[iib][1])/delta
        bbeta[iib][lnodeb]=1.0*(Ab[iib][1]-Ab[iib][3])/delta
        bgamma[iib][lnodeb]=1.0*(Ab[iib][2]-Ab[iib][0])/delta    

slope1=1.0*hy/hx
slope2=-1.0*hy/hx

def greater(val1,val2):
    if val1>val2:
        return True
    elif abs(val1-val2)<=tol:
        return True 
    else:
        return False
    
def less(val1,val2):
    if val1<val2:
        return True
    elif abs(val1-val2)<=tol:
        return True 
    else:
        return False

def ws(nns,mus,xs0,ys0):
    pts=n_s[nns]
    ptsx=x_l[pts][0]
    ptsy=x_l[pts][1]
    if mus==1:
        if greater(xs0,ptsx-hx) and less(xs0, ptsx) and greater(ys0,ptsy-hy) and less(ys0,ptsy) and greater(ys0,slope2*xs0+ptsy-hy-slope2*ptsx):
            result=salpha[mus-1][nns]+sbeta[mus-1][nns]*xs0+sgamma[mus-1][nns]*ys0
        else: 
            result=0.0
    elif mus==2:
        if greater(xs0,ptsx) and less(xs0,ptsx+hx) and greater(ys0,ptsy-hy) and less(ys0,ptsy) and greater(ys0,slope1*xs0+ptsy-slope1*(ptsx+hx)):
            result=salpha[mus-1][nns]+sbeta[mus-1][nns]*xs0+sgamma[mus-1][nns]*ys0
        else:
            result=0.0
    elif mus==3:
        if greater(xs0,ptsx-hx) and less(xs0,ptsx) and greater(ys0,ptsy) and less(ys0,ptsy+hy) and less(ys0,slope1*xs0+ptsy-slope1*(ptsx-hx)):
            result=salpha[mus-1][nns]+sbeta[mus-1][nns]*xs0+sgamma[mus-1][nns]*ys0
        else:
            result=0.0
    elif mus==4:
        if greater(xs0,ptsx) and less(xs0,ptsx+hx) and greater(ys0,ptsy) and less(ys0,ptsy+hy) and less(ys0,slope2*xs0+ptsy-slope2*(ptsx+hx)):
            result=salpha[mus-1][nns]+sbeta[mus-1][nns]*xs0+sgamma[mus-1][nns]*ys0
        else:
            result=0.0
    else:
        result=0.0
    return result


def wb(nnb,mub,xs1,ys1):
    ptb=n_b[nnb]
    ptbx=x_l[ptb][0]
    ptby=x_l[ptb][1]
    if mub==1:
        if greater(xs1,ptbx-hx) and less(xs1,ptbx) and greater(ys1,ptby-hy) and less(ys1,ptby) and greater(ys1,slope1*xs1+ptby-slope1*ptbx):
            result=balpha[mub-1][nnb]+bbeta[mub-1][nnb]*xs1+bgamma[mub-1][nnb]*ys1
        else:
            result=0.0
    elif mub==2:
        if greater(xs1,ptbx-hx) and less(xs1,ptbx) and greater(ys1,ptby-hy) and less(ys1,ptby) and less(ys1,slope1*xs1+ptby-slope1*ptbx):
            result=balpha[mub-1][nnb]+bbeta[mub-1][nnb]*xs1+bgamma[mub-1][nnb]*ys1
        else:
            result=0.0
    elif mub==3:
        if greater(xs1,ptbx) and less(xs1,ptbx+hx) and greater(ys1,ptby-hy) and less(ys1,ptby) and less(ys1,slope2*xs1+ptby-slope2*ptbx):
            result=balpha[mub-1][nnb]+bbeta[mub-1][nnb]*xs1+bgamma[mub-1][nnb]*ys1
        else:
            result=0.0
    elif mub==4:
        if greater(xs1,ptbx) and less(xs1,ptbx+hx) and greater(ys1,ptby-hy) and less(ys1,ptby) and greater(ys1,slope2*xs1+ptby-slope2*ptbx):
            result=balpha[mub-1][nnb]+bbeta[mub-1][nnb]*xs1+bgamma[mub-1][nnb]*ys1
        else:
            result=0.0
    elif mub==5:
        if greater(xs1,ptbx) and less(xs1,ptbx+hx) and greater(ys1,ptby) and less(ys1,ptby+hy) and less(ys1,slope1*xs1+ptby-slope1*ptbx):
            result=balpha[mub-1][nnb]+bbeta[mub-1][nnb]*xs1+bgamma[mub-1][nnb]*ys1
        else:
            result=0.0
    elif mub==6:
        if greater(xs1,ptbx) and less(xs1,ptbx+hx) and greater(ys1,ptby) and less(ys1,ptby+hy) and greater(ys1,slope1*xs1+ptby-slope1*ptbx):
            result=balpha[mub-1][nnb]+bbeta[mub-1][nnb]*xs1+bgamma[mub-1][nnb]*ys1
        else:
            result=0.0
    elif mub==7:
        if greater(xs1,ptbx-hx) and less(xs1,ptbx) and greater(ys1,ptby) and less(ys1,ptby+hy) and greater(ys1,slope2*xs1+ptby-slope2*ptbx):
            result=balpha[mub-1][nnb]+bbeta[mub-1][nnb]*xs1+bgamma[mub-1][nnb]*ys1
        else:
            result=0.0
    elif mub==8:
        if greater(xs1,ptbx-hx) and less(xs1,ptbx) and greater(ys1,ptby) and less(ys1,ptby+hy) and less(ys1,slope2*xs1+ptby-slope2*ptbx):
            result=balpha[mub-1][nnb]+bbeta[mub-1][nnb]*xs1+bgamma[mub-1][nnb]*ys1
        else:
            result=0.0
    return result

def phi(l0,xs1,ys1):
    ptsx=x_l[l0-1][0]
    ptsy=x_l[l0-1][1]
    ptbx=x_l[l0-1][0]
    ptby=x_l[l0-1][1]
    for ias in range(ns):
        its=n_s[ias]
        if l0-1==its:
            if greater(xs1,ptsx-hx) and less(xs1, ptsx) and greater(ys1,ptsy-hy) and less(ys1,ptsy) and greater(ys1,slope2*xs1+ptsy-hy-slope2*ptsx):
                return ws(ias,1,xs1,ys1)
            elif greater(xs1,ptsx) and less(xs1,ptsx+hx) and greater(ys1,ptsy-hy) and less(ys1,ptsy) and greater(ys1,slope1*xs1+ptsy-slope1*(ptsx+hx)):
                return ws(ias,2,xs1,ys1)
            elif greater(xs1,ptsx-hx) and less(xs1,ptsx) and greater(ys1,ptsy) and less(ys1,ptsy+hy) and less(ys1,slope1*xs1+ptsy-slope1*(ptsx-hx)):
                return ws(ias,3,xs1,ys1)
            elif greater(xs1,ptsx) and less(xs1,ptsx+hx) and greater(ys1,ptsy) and less(ys1,ptsy+hy) and less(ys1,slope2*xs1+ptsy-slope2*(ptsx+hx)):
                return ws(ias,4,xs1,ys1)
            else:
                return 0.0

        
    for ibs in range(nb):
        itb=n_b[ibs]
        if l0-1==itb:
            if greater(xs1,ptbx-hx) and less(xs1,ptbx) and greater(ys1,ptby-hy) and less(ys1,ptby) and greater(ys1,slope1*xs1+ptby-slope1*ptbx):
                return wb(ibs,1,xs1,ys1)
            elif greater(xs1,ptbx-hx) and less(xs1,ptbx) and greater(ys1,ptby-hy) and less(ys1,ptby) and less(ys1,slope1*xs1+ptby-slope1*ptbx):
                return wb(ibs,2,xs1,ys1)
            elif greater(xs1,ptbx) and less(xs1,ptbx+hx) and greater(ys1,ptby-hy) and less(ys1,ptby) and less(ys1,slope2*xs1+ptby-slope2*ptbx):
                return wb(ibs,3,xs1,ys1)
            elif greater(xs1,ptbx) and less(xs1,ptbx+hx) and greater(ys1,ptby-hy) and less(ys1,ptby) and greater(ys1,slope2*xs1+ptby-slope2*ptbx):
                return wb(ibs,4,xs1,ys1)
            elif greater(xs1,ptbx) and less(xs1,ptbx+hx) and greater(ys1,ptby) and less(ys1,ptby+hy) and less(ys1,slope1*xs1+ptby-slope1*ptbx):
                return wb(ibs,5,xs1,ys1)
            elif greater(xs1,ptbx) and less(xs1,ptbx+hx) and greater(ys1,ptby) and less(ys1,ptby+hy) and greater(ys1,slope1*xs1+ptby-slope1*ptbx):
                return wb(ibs,6,xs1,ys1)
            elif greater(xs1,ptbx-hx) and less(xs1,ptbx) and greater(ys1,ptby) and less(ys1,ptby+hy) and greater(ys1,slope2*xs1+ptby-slope2*ptbx):
                return wb(ibs,7,xs1,ys1)
            elif greater(xs1,ptbx-hx) and less(xs1,ptbx) and greater(ys1,ptby) and less(ys1,ptby+hy) and less(ys1,slope2*xs1+ptby-slope2*ptbx):
                return wb(ibs,8,xs1,ys1)
            else:
                return 0.0


def dis(a1,a2,b1,b2):
    sq=1.0*(a1-b1)**2+(a2-b2)**2
    distance=math.sqrt(sq)
    return distance

K=np.zeros((n,n))
for ios in range(ns):
    los=n_s[ios]
    K[los][los]=4.0

for iob in range(nb):
    lob=n_b[iob]
    K[lob][lob]=4.0
    
for ixx in range(n):
    for iyy in range(n):
        pp1=x_l[ixx][0]
        pp2=x_l[ixx][1]
        qq1=x_l[iyy][0]
        qq2=x_l[iyy][1]
        di=dis(pp1,pp2,qq1,qq2)
        if abs(di-hx)<=tol or abs(di-hy)<=tol:
            K[ixx][iyy]=-1.0
            
print "K=", K

a_ps=1.0*(hx*hy/2.0)*4
a_pb=1.0*(hx*hy/2.0)*8

bbr=np.zeros(n)
for iss in range(ns):
    isl=n_s[iss]
    px1=x_l[isl][0]
    py1=x_l[isl][1]
    fft1=f(px1,py1)
    bbr[isl]=1.0/3.0*fft1*a_ps

for ibb in range(nb):
    ibl=n_b[ibb]
    px2=x_l[ibl][0]
    py2=x_l[ibl][1]
    fft=f(px2,py2)
    bbr[ibl]=1.0/3.0*fft*a_pb
print "rhs=", bbr

c=np.zeros(n)
c=np.linalg.solve(K, bbr)
print "c=", c

def app(x2,y2):
    final=0.0
    for i in range(n):
        final=c[i]*phi(i+1,x2,y2)+final
    return final
def gauss(Ag,bg,Ng,igg):
    M=np.diag(Ag)
    D=np.diagflat(M)
    U=np.triu(Ag,k=1)
    L=np.tril(Ag,k=-1)
    T=np.dot(np.linalg.inv(D+L),-1.0*U)
    cg=np.dot(np.linalg.inv(D+L),bg)
    for ig in range(Ng):
        x=np.dot(T,igg)+cg
        igg=x
    return x

def jacobi(Aj,bj,Nj,igj):
    M=np.diag(Aj)
    D=np.diagflat(M)
    R=Aj-D      #L+U
    T=np.dot(np.linalg.inv(D),-1.0*R)
    cj=np.dot(np.linalg.inv(D),bj)
    for ij in range(Nj):
        x=np.dot(T,igj)+cj
        igj=x
    return x

def SOR(As,bs,ws,igs,Ns):
    M=np.diag(As)
    D=np.diagflat(M)
    U=np.triu(As,k=1)
    L=np.tril(As,k=-1)
    use1=np.linalg.inv(D+ws*L)
    use2=(1.0-ws)*D-ws*U
    Tw=np.dot(use1,use2)
    cw=np.dot(ws*use1,bs)
    for iss in range(Ns):
        x=np.dot(Tw,igs)+cw
        igs=x
    return x

nuse=n+1
def interpolation(nint):   #interpolation from 2h to h 
    n1=nint
    n2=int(nint/2)
    I = np.zeros((n1-1, n2-1))
    for k in range(int(n1-3)):
        if k%2==0:
            I[k][k/2]=1
            I[k+1][k/2]=2
            I[k+2][k/2]=1
    I=1.0/2.0*I
    return I


def restriction(nint):  #restriction from h to 2h
    Iint=interpolation(nint)
    cres=1.0
    R=cres*np.transpose(Iint)
    return R

def vcycle(nf,Ah,bh,igh,nu):
    n1=nf
    n2=int(nf/2)
    I =interpolation(n1)
    R=restriction(n1)
    vh=gauss(Ah,bh,nu,igh)
    resh=bh-np.dot(Ah,vh)
    res2h=np.dot(R,resh)
    uses=np.dot(R,Ah)
    A2h=np.dot(uses,I)
    e2h=np.linalg.solve(A2h,res2h)
    eh=np.dot(I,e2h)
    start=vh+eh
    final=gauss(Ah,bh,nu,start)
    return final

def pointdown(nd,Ahd,bhd,vhd,nu1):   #the thing happens at 2h from h data
    I=interpolation(nd)
    R=restriction(nd)
    n1=int(nd/2)
    uses=np.dot(R,Ahd)
    A2h=np.dot(uses,I)
    resh=bhd-np.dot(Ahd,vhd)
    res2h=np.dot(R,resh)
    guess=np.zeros(n1-1)
    guess1=np.transpose(guess)
    e2h=gauss(A2h,res2h,nu1,guess1)
#    e2h=np.linalg.solve(A2h,res2h)
    return {'A2h':A2h, 'f2h':res2h, 'v2h':e2h}
    
def coarest(nc,Ahc,bhc,vhc,nu1):   #the thing happens at 2h from h data
    I=interpolation(nc)
    R=restriction(nc)
    n1=int(nc/2)
    uses=np.dot(R,Ahc)
    A2h=np.dot(uses,I)
    resh=bhc-np.dot(Ahc,vhc)
    res2h=np.dot(R,resh)
    guess=np.zeros(n1-1)
    guess1=np.transpose(guess)
#    e2h=jacobi(A2h,res2h,guess1,nu1)
    e2h=np.linalg.solve(A2h,res2h)
    return {'A2h':A2h, 'f2h':res2h, 'v2h':e2h}  

def pointup(nu,e2h,vold,Apt,fptu,nu2):   #the thing happens at h from 2hdata
    I=interpolation(nu)
    R=restriction(nu)
    eh=np.dot(I,e2h)
    start=vold+eh
    final=gauss(Apt,fptu,nu2,start)
    return final

def Vcycle(nv,Ah,bh,igh,num,nu1,nu2):     #2**num=coarest
    vh=gauss(Ah,bh,nu1,igh)
    nvec=np.zeros(num+1)
    listA=[None for _ in range(num)]       #listA=[Ah,A2h,A4h..]not include the coarest A
    listf=[None for _ in range(num)]
    listv=[None for _ in range(num)]
    for iy in range(num+1):
        nvec[iy]=nv/(2**iy)
    Wh=Ah
    fh=bh
    listA[0]=Ah
    listf[0]=bh
    listv[0]=vh
    guessh=vh
    for ly in range(1,num+1):
        if ly< num:
            downstep=pointdown(nvec[ly-1],Wh,fh,guessh,nu1)
            listA[ly]=downstep['A2h']
            listf[ly]=downstep['f2h']
            listv[ly]=downstep['v2h']
            Wh=listA[ly]
            fh=listf[ly]
            guessh=listv[ly]
        else:        #at coarest gird
            downstep=coarest(nvec[ly-1],Wh,fh,guessh,nu1)
            vcos=downstep['v2h']
            Acos=downstep['A2h']
            fcos=downstep['f2h']
    e2h=vcos
    for t in range(num-1,-1,-1):
        upstep=pointup(nvec[t],e2h,listv[t],listA[t],listf[t],nu2)
        e2h=upstep
    return e2h
    

nu1=15
nu2=15
num=3
igh=np.zeros(nuse-1)
igh=np.transpose(igh)
gh=Vcycle(nuse,K,bbr,igh,num,nu1,nu2)
print"solution after a Vcycle=", gh

def FMG(nm,Ah,bh,num,nu,nu1,nu2):
    listI=[None for _ in range(num)]
    listR=[None for _ in range(num)]
    listb=[None for _ in range(num+1)]     #listb=[bh,b2h,b4h,b8h...]   include the coarest  just projection of hh
    listA=[None for _ in range(num+1)]     #listA=[Ah,A2h,A4h,A8h...]  include the coarest
    listb[0]=bh
    listA[0]=Ah
    nvec=np.zeros(num+1)
    for iu in range(num+1):
        nvec[iu]=nm/(2**iu)
    fh=bh
    Wh=Ah
    for s in range(num):
        nn=nvec[s]
        I=interpolation(nn)
        R=restriction(nn)
        bl=np.dot(R,fh)
        uses=np.dot(R,Wh)
        A2h=np.dot(uses,I)
        listI[s]=I
        listR[s]=R
        listb[s+1]=bl
        listA[s+1]=A2h
        fh=bl
        Wh=A2h
    nco=nvec[num]
    Aco=listA[num]
    bco=listb[num]
    uco=np.linalg.solve(Aco,bco)      #solve at coarest grid
    vce=np.dot(listI[num-1],uco)      #second last coarest
    for ite in range(nu-1):
        uce=vcycle(nvec[num-1],listA[num-1],listb[num-1],vce,nu1)
        vce=uce
    uww=uce
    for k in range(num-2,-1,-1):
        vguess=np.dot(listI[k],uww)
        for ita in range(nu-1):
            unew=Vcycle(nvec[k],listA[k],listb[k],vguess,num-k,nu1,nu2)
            vguess=unew
        uww=unew
    return unew

num=3
nu=20
nu1=15
nu2=15
ggh=FMG(nuse,K,bbr,num,nu,nu1,nu2)
print"solution after a full multigrid=", ggh

mutd=ggh
def appmut(xmu,ymu):
    final=0.0
    for imu in range(n):
        final=mutd[imu]*phi(imu+1,xmu,ymu)+final
    return final

def exact(xt,yt):
    return (xt**2-3*xt)*(yt**2-3*yt)

print "solution with direct solver at (1.0,2.25)=", app(1.0,2.25)
print "solution with multigrid at (1.0,2.25)=", appmut(1.0,2.25)
print "exact solution at (1.0,2.25)=", exact(1.0,2.25)

def inte(ap,bp,fp):
    deg = 5
    pt, wei = np.polynomial.legendre.leggauss(deg)
    sume=0
    for j in range(deg):
        xxy=((bp-ap)*pt[j]+(bp+ap))/2.0
        sume=wei[j]*fp(xxy)*(bp-ap)/2.0+sume
    return sume
    
        
def muint(ap,bp,cp,dp,fp):
    deg = 5
    pt, wei = np.polynomial.legendre.leggauss(deg)
    def ffuse(xw):
        deg = 11
        pt, wei = np.polynomial.legendre.leggauss(deg)
        first=(dp(xw)-cp(xw))/2.0
        summ=0
        for j in range(deg):
            yy=((dp(xw)-cp(xw))*pt[j]+dp(xw)+cp(xw))/2.0
            summ=wei[j]*fp(xw,yy)+summ
        rest=first*summ
        return rest
    final=inte(ap,bp,ffuse)
    return final

def diffeapp(xd1,yd1):
    return (exact(xd1,yd1)-app(xd1,yd1))**2

def diffemul(xd2,yd2):
    return (exact(xd2,yd2)-appmut(xd2,yd2))**2

def diffappmul(xd3,yd3):
    return (app(xd3,yd3)-appmut(xd3,yd3))**2

intsum1=0.0
intsum2=0.0
intsum3=0.0
for ir in range(nx):
    for jr in range(ny):
        aa1=a+1.0*ir*hx
        bb1=a+1.0*(ir+1)*hx
        if (ir+jr)%2==0:
            def cc1(xo1):
                return b+1.0*jr*hy
            def dd1(xo2):
                return slope2*xo2+b+1.0*jr*hy-slope2*(a+1.0*(ir+1)*hx) 
            def cc2(xo3):
                return slope2*xo3+b+1.0*jr*hy-slope2*(a+1.0*(ir+1)*hx) 
            def dd2(xo4):
                return b+1.0*(jr+1)*hy
            T11=muint(aa1,bb1,cc1,dd1,diffeapp)
            T21=muint(aa1,bb1,cc2,dd2,diffeapp)
            T12=muint(aa1,bb1,cc1,dd1,diffemul)
            T22=muint(aa1,bb1,cc2,dd2,diffemul)
            T13=muint(aa1,bb1,cc1,dd1,diffappmul)
            T23=muint(aa1,bb1,cc2,dd2,diffappmul)
        elif (ir+jr)%2==1:
            def cy1(xo5):
                return b+1.0*jr*hy
            def dy1(xo6):
                return slope1*xo6+b+1.0*jr*hy-slope1*(a+1.0*(ir)*hx) 
            def cy2(xo7):
                return slope1*xo7+b+1.0*jr*hy-slope1*(a+1.0*(ir)*hx) 
            def dy2(xo8):
                return b+1.0*(jr+1)*hy
            T11=muint(aa1,bb1,cy1,dy1,diffeapp)
            T21=muint(aa1,bb1,cy2,dy2,diffeapp)
            T12=muint(aa1,bb1,cy1,dy1,diffemul)
            T22=muint(aa1,bb1,cy2,dy2,diffemul)
            T13=muint(aa1,bb1,cy1,dy1,diffappmul)
            T23=muint(aa1,bb1,cy2,dy2,diffappmul)
        intsum1=intsum1+T11+T21
        intsum2=intsum2+T12+T22
        intsum3=intsum3+T13+T23
norm1=math.sqrt(intsum1)
norm2=math.sqrt(intsum2)
norm3=math.sqrt(intsum3)
print "L2 norm of uh-u=", norm1
print "L2 norm of uhg-u=", norm2
print "L2 norm of uhg-uh=", norm3