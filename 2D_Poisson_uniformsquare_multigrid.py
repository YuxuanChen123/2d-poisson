import numpy as np
import matplotlib.pyplot as plt
import sys
import matplotlib
from sympy import cot
import math

a=0.0
b=0.0
lx=3.0
ly=3.0
nx=10
ny=8
hx=1.0*lx/(nx)
hy=1.0*ly/(ny)

def f(x1,y1):
    return -2.0*(x1**2+y1**2-3*x1-3*y1)

def r(x1,y1):
    return 0.0

n=(nx-1)*(ny-1)
print "n=",n 
print "hx=", hx
print "hy=", hy

kk=0
x_l=np.zeros((n,2))
for ix in range(1,nx):
    for iy in range(1,ny):
        x_l[kk][0]=a+1.0*ix*hx
        x_l[kk][1]=b+1.0*iy*hy
        kk=kk+1

alpha=np.zeros((6,n))
beta=np.zeros((6,n))
gamma=np.zeros((6,n))

areat=(hx*hy)/2.0
delta=2.0*areat

for l in range(n):
    A=np.zeros((6,4))
    #first triangle
    A[0][0]=x_l[l][0]-hx
    A[0][1]=x_l[l][1]
    A[0][2]=x_l[l][0]-hx
    A[0][3]=x_l[l][1]-hy
    #second triangle
    A[1][0]=x_l[l][0]-hx
    A[1][1]=x_l[l][1]-hy
    A[1][2]=x_l[l][0]
    A[1][3]=x_l[l][1]-hy
    #third triangle
    A[2][0]=x_l[l][0]
    A[2][1]=x_l[l][1]-hy
    A[2][2]=x_l[l][0]+hx
    A[2][3]=x_l[l][1]
    #fourth triangle
    A[3][0]=x_l[l][0]+hx
    A[3][1]=x_l[l][1]
    A[3][2]=x_l[l][0]+hx
    A[3][3]=x_l[l][1]+hy
    #fifth triangle
    A[4][0]=x_l[l][0]+hx
    A[4][1]=x_l[l][1]+hy
    A[4][2]=x_l[l][0]
    A[4][3]=x_l[l][1]+hy
    #sixth triangle
    A[5][0]=x_l[l][0]
    A[5][1]=x_l[l][1]+hy
    A[5][2]=x_l[l][0]-hx
    A[5][3]=x_l[l][1]
    for i in range(6):
        alpha[i][l]=(A[i][0]*A[i][3]-A[i][2]*A[i][1])/delta
        beta[i][l]=(A[i][1]-A[i][3])/delta
        gamma[i][l]=(A[i][2]-A[i][0])/delta

slope=hy/hx

def w(ll,muu,x5,y5):
    p=x_l[ll-1][0]
    q=x_l[ll-1][1]
    if muu==1:
        if ((x5>=p-hx) and (x5<=p) and (y5>=q-hy) and (y5<=q) and (y5>=slope*x5+q-slope*p)):
            result=alpha[muu-1][ll-1]+beta[muu-1][ll-1]*x5+gamma[muu-1][ll-1]*y5
        else: 
            result=0.0
    elif muu==2:
        if x5>=p-hx and x5<=p and y5>=q-hy and y5<=q and y5<=slope*x5+q-slope*p:
            result=alpha[muu-1][ll-1]+beta[muu-1][ll-1]*x5+gamma[muu-1][ll-1]*y5
        else:
            result=0.0
    elif muu==3:
        if x5>=p and x5<=p+hx and y5>=q-hy and y5<=q and y5>=slope*x5+q-slope*(p+hx):
            result=alpha[muu-1][ll-1]+beta[muu-1][ll-1]*x5+gamma[muu-1][ll-1]*y5
        else:
            result=0.0
    elif muu==4:
        if x5>=p and x5<=p+hx and y5>=q and y5<=q+hy and y5<=slope*x5+q-slope*p:
            result=alpha[muu-1][ll-1]+beta[muu-1][ll-1]*x5+gamma[muu-1][ll-1]*y5
        else:
            result=0.0
    elif muu==5:
        if x5>=p and x5<=p+hx and y5>=q and y5<=q+hy and y5>=slope*x5+q-slope*p:
            result=alpha[muu-1][ll-1]+beta[muu-1][ll-1]*x5+gamma[muu-1][ll-1]*y5
        else:
            result=0.0
    elif muu==6:
        if x5>=p-hx and x5<=p and y5>=q and y5<=q+hy and y5<=slope*x5+q-slope*(p-hx):
            result=alpha[muu-1][ll-1]+beta[muu-1][ll-1]*x5+gamma[muu-1][ll-1]*y5
        else:
            result=0.0
    else:
        result=0.0
    return result

def phi(l0,x0,y0):
    p=x_l[l0-1][0]
    q=x_l[l0-1][1]
    if x0>=p-hx and x0<=p and y0>=q-hy and y0<=q and y0>=slope*x0+q-slope*p:
        return w(l0,1,x0,y0)
    elif x0>=p-hx and x0<=p and y0>=q-hy and y0<=q and y0<=slope*x0+q-slope*p:
        return w(l0,2,x0,y0)
    elif x0>=p and x0<=p+hx and y0>=q-hy and y0<=q and y0>=slope*x0+q-slope*(p+hx):
        return w(l0,3,x0,y0)
    elif x0>=p and x0<=p+hx and y0>=q and y0<=q+hy and y0<=slope*x0+q-slope*p:
        return w(l0,4,x0,y0)
    elif x0>=p and x0<=p+hx and y0>=q and y0<=q+hy and y0>=slope*x0+q-slope*p:
        return w(l0,5,x0,y0)
    elif x0>=p-hx and x0<=p and y0>=q and y0<=q+hy and y0<=slope*x0+q-slope*(p-hx):
        return w(l0,6,x0,y0)
    else:
        return 0.0
    

tol=10**-5

def dis(a1,a2,b1,b2):
    sq=1.0*(a1-b1)**2+(a2-b2)**2
    distance=math.sqrt(sq)
    return distance


K=np.zeros((n,n))
for ik in range(n):
    for jk in range(n):
        p1=x_l[ik][0]
        p2=x_l[ik][1]
        q1=x_l[jk][0]
        q2=x_l[jk][1]
        di=dis(p1,p2,q1,q2)
        if ik!=jk:
            if abs(di-hx)<tol or abs(di-hy)<tol:
                K[ik][jk]=-1.0
            elif abs(di-math.sqrt(hx**2+hy**2))<tol:
                K[ik][jk]=0.0
        elif ik==jk:
            K[ik][ik]=4.0

            
print "K=", K

a_p=(hx*hy/2.0)*6
bb=np.zeros(n)
for ib in range(n):
    p1=x_l[ib][0]
    p2=x_l[ib][1]
    fft=f(p1,p2)
    bb[ib]=1.0/3.0*fft*a_p
print "bb=", bb

c0=np.zeros(n)
c0=np.linalg.solve(K, bb)
print "c=", c0
def app(xap,yap):
    final=0.0
    for iap in range(n):
        final=c0[iap]*phi(iap+1,xap,yap)+final
    return final


def gauss(Ag,bg,igg,Ng):
    M=np.diag(Ag)
    D=np.diagflat(M)
    U=np.triu(Ag,k=1)
    L=np.tril(Ag,k=-1)
    T=np.dot(np.linalg.inv(D+L),-1.0*U)
    cg=np.dot(np.linalg.inv(D+L),bg)
    for ig in range(Ng):
        x=np.dot(T,igg)+cg
        igg=x
    return x

def jacobi(Aj,bj,Nj,igj):
    M=np.diag(Aj)
    D=np.diagflat(M)
    R=Aj-D      #L+U
    T=np.dot(np.linalg.inv(D),-1.0*R)
    cj=np.dot(np.linalg.inv(D),bj)
    for ij in range(Nj):
        x=np.dot(T,igj)+cj
        igj=x
    return x

def SOR(As,bs,ws,igs,Ns):
    M=np.diag(As)
    D=np.diagflat(M)
    U=np.triu(As,k=1)
    L=np.tril(As,k=-1)
    use1=np.linalg.inv(D+ws*L)
    use2=(1.0-ws)*D-ws*U
    Tw=np.dot(use1,use2)
    cw=np.dot(ws*use1,bs)
    for iss in range(Ns):
        x=np.dot(Tw,igs)+cw
        igs=x
    return x


nuse=n+1
def interpolation(nint):   #interpolation from 2h to h 
    n1=nint
    n2=int(nint/2)
    I = np.zeros((n1-1, n2-1))
    for k in range(int(n1-3)):
        if k%2==0:
            I[k][k/2]=1
            I[k+1][k/2]=2
            I[k+2][k/2]=1
    I=1.0/2.0*I
    return I


def restriction(nint):  #restriction from h to 2h
    Iint=interpolation(nint)
    cres=1.0
    R=cres*np.transpose(Iint)
    return R

def vcycle(nf,Ah,bh,igh,nu):
    n1=nf
    n2=int(nf/2)
    I =interpolation(n1)
    R=restriction(n1)
    vh=jacobi(Ah,bh,nu,igh)
    resh=bh-np.dot(Ah,vh)
    res2h=np.dot(R,resh)
    uses=np.dot(R,Ah)
    A2h=np.dot(uses,I)
    e2h=np.linalg.solve(A2h,res2h)
    eh=np.dot(I,e2h)
    start=vh+eh
    final=jacobi(Ah,bh,nu,start)
    return final
def pointdown(nd,Ahd,bhd,vhd,nu1):   #the thing happens at 2h from h data
    I=interpolation(nd)
    R=restriction(nd)
    n1=int(nd/2)
    uses=np.dot(R,Ahd)
    A2h=np.dot(uses,I)
    resh=bhd-np.dot(Ahd,vhd)
    res2h=np.dot(R,resh)
    guess=np.zeros(n1-1)
    guess1=np.transpose(guess)
    e2h=jacobi(A2h,res2h,nu1,guess1)
#    e2h=np.linalg.solve(A2h,res2h)
    return {'A2h':A2h, 'f2h':res2h, 'v2h':e2h}
    
def coarest(nc,Ahc,bhc,vhc,nu1):   #the thing happens at 2h from h data
    I=interpolation(nc)
    R=restriction(nc)
    n1=int(nc/2)
    uses=np.dot(R,Ahc)
    A2h=np.dot(uses,I)
    resh=bhc-np.dot(Ahc,vhc)
    res2h=np.dot(R,resh)
    guess=np.zeros(n1-1)
    guess1=np.transpose(guess)
#    e2h=jacobi(A2h,res2h,guess1,nu1)
    e2h=np.linalg.solve(A2h,res2h)
    return {'A2h':A2h, 'f2h':res2h, 'v2h':e2h}  

def pointup(nu,e2h,vold,Apt,fptu,nu2):   #the thing happens at h from 2hdata
    I=interpolation(nu)
    R=restriction(nu)
    eh=np.dot(I,e2h)
    start=vold+eh
    final=jacobi(Apt,fptu,nu2,start)
    return final

def Vcycle(nv,Ah,bh,igh,num,nu1,nu2):     #2**num=coarest
    vh=jacobi(Ah,bh,nu1,igh)
    nvec=np.zeros(num+1)
    listA=[None for _ in range(num)]       #listA=[Ah,A2h,A4h..]not include the coarest A
    listf=[None for _ in range(num)]
    listv=[None for _ in range(num)]
    for iy in range(num+1):
        nvec[iy]=nv/(2**iy)
    Wh=Ah
    fh=bh
    listA[0]=Ah
    listf[0]=bh
    listv[0]=vh
    guessh=vh
    for ly in range(1,num+1):
        if ly< num:
            downstep=pointdown(nvec[ly-1],Wh,fh,guessh,nu1)
            listA[ly]=downstep['A2h']
            listf[ly]=downstep['f2h']
            listv[ly]=downstep['v2h']
            Wh=listA[ly]
            fh=listf[ly]
            guessh=listv[ly]
        else:        #at coarest gird
            downstep=coarest(nvec[ly-1],Wh,fh,guessh,nu1)
            vcos=downstep['v2h']
            Acos=downstep['A2h']
            fcos=downstep['f2h']
    e2h=vcos
    for t in range(num-1,-1,-1):
        upstep=pointup(nvec[t],e2h,listv[t],listA[t],listf[t],nu2)
        e2h=upstep
    return e2h
    

nu1=15
nu2=15
num=3
igh=np.zeros(nuse-1)
igh=np.transpose(igh)
gh=Vcycle(nuse,K,bb,igh,num,nu1,nu2)
print "after a V cycle, solution=", gh


def FMG(nm,Ah,bh,num,nu,nu1,nu2):
    listI=[None for _ in range(num)]
    listR=[None for _ in range(num)]
    listb=[None for _ in range(num+1)]     #listb=[bh,b2h,b4h,b8h...]   include the coarest  just projection of hh
    listA=[None for _ in range(num+1)]     #listA=[Ah,A2h,A4h,A8h...]  include the coarest
    listb[0]=bh
    listA[0]=Ah
    nvec=np.zeros(num+1)
    for iu in range(num+1):
        nvec[iu]=nm/(2**iu)
    fh=bh
    Wh=Ah
    for s in range(num):
        nn=nvec[s]
        I=interpolation(nn)
        R=restriction(nn)
        bl=np.dot(R,fh)
        uses=np.dot(R,Wh)
        A2h=np.dot(uses,I)
        listI[s]=I
        listR[s]=R
        listb[s+1]=bl
        listA[s+1]=A2h
        fh=bl
        Wh=A2h
    nco=nvec[num]
    Aco=listA[num]
    bco=listb[num]
    uco=np.linalg.solve(Aco,bco)      #solve at coarest grid
    vce=np.dot(listI[num-1],uco)      #second last coarest
    for ite in range(nu-1):
        uce=vcycle(nvec[num-1],listA[num-1],listb[num-1],vce,nu1)
        vce=uce
    uww=uce
    for k in range(num-2,-1,-1):
        vguess=np.dot(listI[k],uww)
        for ita in range(nu-1):
            unew=Vcycle(nvec[k],listA[k],listb[k],vguess,num-k,nu1,nu2)
            vguess=unew
        uww=unew
    return unew

num=3
nu=20
nu1=15
nu2=15
ggh=FMG(nuse,K,bb,num,nu,nu1,nu2)

print "after a full multigrid, the solution=", ggh


mutd=ggh
def appmut(xmu,ymu):
    final=0.0
    for imu in range(n):
        final=mutd[imu]*phi(imu+1,xmu,ymu)+final
    return final

def exact(xt,yt):
    return (xt**2-3*xt)*(yt**2-3*yt)

def inte(ap,bp,fp):
    deg = 5
    pt, wei = np.polynomial.legendre.leggauss(deg)
    sume=0
    for j in range(deg):
        xxy=((bp-ap)*pt[j]+(bp+ap))/2.0
        sume=wei[j]*fp(xxy)*(bp-ap)/2.0+sume
    return sume
    
        
def muint(ap,bp,cp,dp,fp):
    deg = 5
    pt, wei = np.polynomial.legendre.leggauss(deg)
    def ffuse(xw):
        deg = 11
        pt, wei = np.polynomial.legendre.leggauss(deg)
        first=(dp(xw)-cp(xw))/2.0
        summ=0
        for j in range(deg):
            yy=((dp(xw)-cp(xw))*pt[j]+dp(xw)+cp(xw))/2.0
            summ=wei[j]*fp(xw,yy)+summ
        rest=first*summ
        return rest
    final=inte(ap,bp,ffuse)
    return final

def diffeapp(xd1,yd1):
    return (exact(xd1,yd1)-app(xd1,yd1))**2

def diffemul(xd2,yd2):
    return (exact(xd2,yd2)-appmut(xd2,yd2))**2

def diffappmul(xd3,yd3):
    return (app(xd3,yd3)-appmut(xd3,yd3))**2


intsum1=0.0
intsum2=0.0
intsum3=0.0
for ir in range(nx):
    for jr in range(ny):
        aa1=a+1.0*ir*hx
        bb1=a+1.0*(ir+1)*hx
        def cc1(xo1):
            return b+1.0*jr*hy
        def dd1(xo2):
            return slope*xo2+b+1.0*jr*hy-slope*(a+1.0*ir*hx) 
        def cc2(xo3):
            return slope*xo3+b+1.0*jr*hy-slope*(a+1.0*ir*hx)
        def dd2(xo4):
            return b+1.0*(jr+1)*hy
        T11=muint(aa1,bb1,cc1,dd1,diffeapp)
        T21=muint(aa1,bb1,cc2,dd2,diffeapp)
        T12=muint(aa1,bb1,cc1,dd1,diffemul)
        T22=muint(aa1,bb1,cc2,dd2,diffemul)   
        T13=muint(aa1,bb1,cc1,dd1,diffappmul)
        T23=muint(aa1,bb1,cc2,dd2,diffappmul)   
        intsum1=intsum1+T11+T21
        intsum2=intsum2+T21+T22
        intsum3=intsum3+T13+T23
print(intsum1)
norm1=math.sqrt(intsum1)
print(norm1)
print(intsum2)
norm2=math.sqrt(intsum2)
print(norm2)
print(intsum3)
norm3=math.sqrt(intsum3)
print(norm3)

print "L2 norm of uh-u=", norm1
print "L2 norm of uhg-u=", norm2
print "L2 norm of uhg-uh=", norm3
